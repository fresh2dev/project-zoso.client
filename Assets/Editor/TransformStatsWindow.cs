﻿using UnityEngine;
using UnityEditor;

public class TransformStatsWindow : EditorWindow
{
	private static readonly string[] _cachedNumbers = new[] { "0", "1", "2", "3" };

	private Transform selection;
	private long nextUpdate;

	[MenuItem("&Utils/Transform Stats...")]
	static void Init()
	{
		var wnd = EditorWindow.GetWindow(typeof(TransformStatsWindow));
		wnd.title = "Transform Stats";
		wnd.minSize = new Vector2(250, 300);
	}

	void Update()
	{
		var newSelection = Selection.activeTransform;
		if (newSelection != selection)
		{
			this.selection = newSelection;
			this.Repaint();
		}

		var ticks = System.DateTime.Now.Ticks;
		if (nextUpdate < ticks)
		{
			this.Repaint();
			this.nextUpdate = ticks + 500 * System.TimeSpan.TicksPerMillisecond; // half a second
		}
	}

	void OnGUI()
	{
		if (selection != null)
		{
			var m2 = selection.worldToLocalMatrix;
			PrintMatrix("World To Local Matrix:", ref m2);

			var m1 = selection.localToWorldMatrix;
			PrintMatrix("Local To World Matrix:", ref m1);

			GUILayout.Space(20);
			EditorGUILayout.Vector3Field("World Position:", selection.position);
			EditorGUILayout.Vector3Field("Local Position:", selection.localPosition);

			GUILayout.Space(20);
			EditorGUILayout.Vector3Field("World Rotation:", selection.eulerAngles);
			EditorGUILayout.Vector3Field("Local Rotation:", selection.localEulerAngles);
		}
		else
		{
			GUILayout.Label("Select an object for stats");
		}
	}

	static void PrintMatrix(string label, ref Matrix4x4 matrix)
	{
		GUILayout.Label(label);
		GUILayout.Space(5);
		EditorGUILayout.BeginHorizontal();
		{
			GUILayout.Space(5);
			GUILayout.Label("x");
			GUILayout.FlexibleSpace();
			GUILayout.Label("0");
			GUILayout.FlexibleSpace();
			GUILayout.Label("1");
			GUILayout.FlexibleSpace();
			GUILayout.Label("2");
			GUILayout.FlexibleSpace();
			GUILayout.Label("3");
		}
		EditorGUILayout.EndHorizontal();

		for (var i = 0; i < 4; i++)
		{
			EditorGUILayout.BeginHorizontal();
			{
				GUILayout.Space(5);
				GUILayout.Label(_cachedNumbers[i]);
				for (var j = 0; j < 4; j++)
				{
					GUILayout.FlexibleSpace();
					EditorGUILayout.FloatField(string.Empty, matrix[i, j], GUILayout.MaxWidth(50));

				}
			}
			EditorGUILayout.EndHorizontal();
		}
		GUILayout.Space(5);
	}
}
