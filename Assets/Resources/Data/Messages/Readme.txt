// Usage:
// comments can be used anywhere on a new line with '\' (forward slash)
// every block should begin with [Block] tag
// anything after [Block] is ignored; it is a good place to put block number
// block numbers should start from 0 and increase numerically
// white spaces are ignored outside the block