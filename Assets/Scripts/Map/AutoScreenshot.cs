﻿using UnityEngine;

using System.Collections;
using System.IO;

public class AutoScreenshot : MonoBehaviour
{
	public float snapDelay = 1.0f;
	public string SegmentName = "seg";
	public float Aspect = 1.0f;
	public int SuperSize = 2;

	IEnumerator Start()
	{
		if (snapDelay < 0.1f)
		{
			this.snapDelay = 0.1f;
			Debug.LogWarning("Snap delay cannot be lower than 0.1 seconds, using 0.1 seconds.");
		}

		GetComponent<Camera>().enabled = true;

		var cameras = Camera.allCameras;
		int activeCameras = 0;

		for (int i = 0; i < cameras.Length; i++)
		{
			if(cameras[i].enabled) activeCameras++;
			if (activeCameras > 1)
			{
				Debug.LogError("Disable all other cameras before running AutoSnapshot");
				yield break;
			}
		}

		GetComponent<Camera>().aspect = Aspect;

		var xHalfUnit = GetComponent<Camera>().orthographicSize * GetComponent<Camera>().aspect;
		var zHalfUnit = GetComponent<Camera>().orthographicSize;

		this.moveCam(xHalfUnit, zHalfUnit);

		var xInc = xHalfUnit * 2;
		var zInc = zHalfUnit * 2;
		
		int xTerrainMax = (int)Terrain.activeTerrain.terrainData.size.x;
		int zTerrainMax = (int)Terrain.activeTerrain.terrainData.size.z;

		ClientHelper.CreateAssetFolderIfNotExists("Minimap/Textures");
		for (float x = 0; x < xTerrainMax + zHalfUnit; x += xInc)
		{
			for (float z = 0; z < zTerrainMax + xHalfUnit; z += zInc)
			{
				this.moveCam(x, z);

				Application.CaptureScreenshot(string.Format("Assets/Minimap/Textures/{0}-{1}.{2}.png", SegmentName, x, z), SuperSize);
				
				yield return new WaitForSeconds(snapDelay);
			}
		}

		using (var writer = new StreamWriter(Application.dataPath + "/Minimap/settings.txt"))
		{
			writer.WriteLine(string.Format("name=\"{0}\"", SegmentName));
			writer.WriteLine(string.Format("length={0}", xInc));
			writer.WriteLine(string.Format("width={0}", zInc));
			writer.WriteLine(string.Format("xMin={0}", 0));
			writer.WriteLine(string.Format("xMax={0}", xTerrainMax));
			writer.WriteLine(string.Format("zMin={0}", 0));
			writer.WriteLine(string.Format("zMax={0}", zTerrainMax));
		}
	}

	void moveCam(float x, float z)
	{
		this.transform.position = new Vector3(x, this.transform.position.y, z);
	}
}
