﻿namespace Karen90MmoFramework.Client
{
	public enum DestroyReason
	{
		AppExit,
		HandlerChanged,
		Disconnect
	}
}
