﻿namespace Karen90MmoFramework.Client
{
	public interface IConnectResponseHandler
	{
		/// <summary>
		/// Called when the server connection has been established
		/// </summary>
		void OnConnected();

		/// <summary>
		/// Called when the server connection has been destroyed
		/// </summary>
		void OnDisconnected();
	}
}
