﻿using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client.Data
{
	public class QuestData
	{
		public short QuestId { get; set; }
		public string Name { get; set; }
		public short Level { get; set; }

		public string QuestIntroMsg { get; set; }
		public string QuestProgressMsg { get; set; }
		public string QuestCompleteMsg { get; set; }

		public string[] Objectives { get; set; }
		public byte[] ObjectiveCounts { get; set; }
		public string FinalObjective { get; set; } // usually quest turn-ins

		public int RewardXp { get; set; }
		public int RewardMoney { get; set; }
		public int RewardRenown { get; set; }
		public short[] RewardSpells { get; set; }
		public ItemStructure[] RewardItems { get; set; }
		public ItemStructure[] RewardOptionalItems { get; set; }
	}
}
