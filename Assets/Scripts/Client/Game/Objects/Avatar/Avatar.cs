﻿using System.Linq;
using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client;
using Karen90MmoFramework.Client.Game.Objects;
using Karen90MmoFramework.Client.Game;

public class Avatar : MonoBehaviour, IAvatar, IBlip
{
	#region Constants and Fields

	private MmoObject owner;
	private CharacterController controller;
	
	private GameObject lootIndicator;

	private Transform poiTransform;
	private Texture2D blipIcon;
	private OverheadIconType poiIconType = OverheadIconType.None;
	private BlipIconType blipIconType = BlipIconType.None;

	protected bool ShowTooltip;

	bool allowInteraction;
	bool hover;

	#endregion

	#region Properties

	/// <summary>
	/// Gets or sets the avatar's name
	/// </summary>
	public virtual string AvatarName { get; set; }

	/// <summary>
	/// Avatar's Position
	/// </summary>
	public Vector3 Position
	{
		get
		{
			return this.transform.position;
		}
	}

	/// <summary>
	/// Avatar's Rotation
	/// </summary>
	public Vector3 Rotation
	{
		get
		{
			return this.transform.rotation.eulerAngles;
		}
	}

	/// <summary>
	/// Owner
	/// </summary>
	public MmoObject Owner
	{
		get
		{
			return this.owner;
		}
	}

	/// <summary>
	/// Gets the center
	/// </summary>
	public virtual Vector3 Center
	{
		get
		{
			return this.Position;
		}
	}

	/// <summary>
	/// Gets the height
	/// </summary>
	public virtual float Height
	{
		get
		{
			return ObjectRenderer.bounds.extents.y * 2;
		}
	}

	/// <summary>
	/// Tells whether this avatar is view or not
	/// </summary>
	protected bool InView { get; private set; }

	/// <summary>
	/// Gets or sets the object renderer
	/// </summary>
	protected Renderer ObjectRenderer { get; set; }

	/// <summary>
	/// Gets the movement state
	/// </summary>
	protected MovementState State { get; private set; }

	/// <summary>
	/// Gets the movement direction
	/// </summary>
	protected MovementDirection Direction { get; private set; }

	#endregion

	#region IBlip Implementation

	/// <summary>
	/// Gets the blip icon
	/// </summary>
	Texture2D IBlip.BlipIcon
	{
		get { return this.blipIcon; }
	}

	/// <summary>
	/// Gets the current position
	/// </summary>
	Vector3 IBlip.Position
	{
		get { return this.Center; }
	}

	#endregion

	#region IAvatar Implementation

	/// <summary>
	/// Sets an Owner
	/// </summary>
	void IAvatar.SetOwner(MmoObject newOwner)
	{
		this.owner = newOwner;
		this.OnSetOwner(newOwner);
	}

	/// <summary>
	/// Spawns the Avatar
	/// </summary>
	void IAvatar.Spawn(Vector3 position)
	{
		((IAvatar)this).SetPosition(position);
		this.OnAvatarSpawn(position);
	}

	/// <summary>
	/// Destroys the Avatar
	/// </summary>
	void IAvatar.Destroy()
	{
		if (poiIconType != OverheadIconType.None)
			((IAvatar)this).SetOverheadIcon(OverheadIconType.None);

		if (blipIconType != BlipIconType.None)
			((IAvatar)this).SetBlipIcon(BlipIconType.None);
		
		this.OnAvatarDestroy();
	}

	/// <summary>
	/// Sets the position
	/// </summary>
	void IAvatar.SetPosition(Vector3 newPosition)
	{
		this.transform.position = newPosition;
		if(controller != null)
			controller.Move(newPosition - transform.position);

		if (blipIconType != BlipIconType.None)
			this.owner.World.Map.UpdatePOI(this);
	}

	/// <summary>
	/// Sets the orientation
	/// </summary>
	void IAvatar.SetRotation(Vector3 newRotation)
	{
		this.transform.rotation = Quaternion.Euler(newRotation);
	}

	/// <summary>
	/// Creates a loot indicator
	/// </summary>
	void IAvatar.CreateLootIndicator()
	{
		var pos = this.transform.position;
		pos.y -= this.GetComponent<Renderer>().bounds.extents.y;
		this.lootIndicator = (GameObject)Instantiate(GameResources.Instance.GoLootEffect, pos, Quaternion.identity);
		this.lootIndicator.transform.parent = this.transform;
	}

	/// <summary>
	/// Clears loot indicator
	/// </summary>
	void IAvatar.ClearLootIndicator()
	{
		if (lootIndicator != null)
		{
			Destroy(lootIndicator);
			this.lootIndicator = null;
		}
	}

	/// <summary>
	/// Sets the overhead icon
	/// </summary>
	/// <param name="iconType"></param>
	void IAvatar.SetOverheadIcon(OverheadIconType iconType)
	{
		Texture2D poiIcon = null;
		switch (iconType)
		{
			case OverheadIconType.QuestActive:
				poiIcon = GameResources.Instance.IconOverheadQuestActive;
				break;

			case OverheadIconType.QuestInProgress:
				poiIcon = GameResources.Instance.IconOverheadQuestInProgress;
				break;

			case OverheadIconType.QuestTurnIn:
				poiIcon = GameResources.Instance.IconOverheadQuestComplete;
				break;

			case OverheadIconType.Merchant:
				poiIcon = GameResources.Instance.IconOverheadMerchant;
				break;
		}

		if (poiIconType != iconType)
		{
			if (poiTransform == null)
			{
				poiTransform = ((GameObject) Instantiate(GameResources.Instance.GoPOIHolder, new Vector3(Center.x, Center.y - .1f, Center.z), Quaternion.identity)).transform;
				poiTransform.parent = this.transform;
			}

			if(iconType == OverheadIconType.None)
			{
				Destroy(poiTransform.gameObject);
				poiTransform = null;
			}
			else
			{
				poiTransform.GetComponent<Renderer>().material.mainTexture = poiIcon;
			}
		}

		this.poiIconType = iconType;
	}

	/// <summary>
	/// Sets the minimap blip icon
	/// </summary>
	/// <param name="iconType"></param>
	void IAvatar.SetBlipIcon(BlipIconType iconType)
	{
		switch (iconType)
		{
			case BlipIconType.GroupMember:
				this.blipIcon = GameResources.Instance.IconBlipGroupMember;
				break;

			case BlipIconType.QuestActive:
				this.blipIcon = GameResources.Instance.IconBlipQuestActive;
				break;

			case BlipIconType.QuestInProgress:
				this.blipIcon = GameResources.Instance.IconBlipQuestInProgress;
				break;

			case BlipIconType.QuestTurnIn:
				this.blipIcon = GameResources.Instance.IconBlipQuestComplete;
				break;

			case BlipIconType.Merchant:
				this.blipIcon = GameResources.Instance.IconBlipMerchant;
				break;

			case BlipIconType.None:
				this.blipIcon = null;
				break;
		}

		if (blipIconType != iconType)
		{
			if (iconType == BlipIconType.None)
			{
				owner.World.Map.RemovePOI(this);
			}
			else
			{
				owner.World.Map.AddPOI(this);
			}
		}

		this.blipIconType = iconType;
	}

	/// <summary>
	/// Allows interaction to this object.
	/// </summary>
	void IAvatar.AllowInteraction()
	{
		this.allowInteraction = true;
	}

	/// <summary>
	/// Denies interaction to this object.
	/// </summary>
	void IAvatar.DenyInteraction()
	{
		this.allowInteraction = false;
		if (hover)
		{
			this.OnEndHighlight();
			this.hover = false;
		}
	}

	/// <summary>
	/// Sets the movement state
	/// </summary>
	void IAvatar.SetState(MovementState state)
	{
		this.State = state;
		this.UpdateAnimationState();
	}

	/// <summary>
	/// Sets the movement direction
	/// </summary>
	void IAvatar.SetDirection(MovementDirection direction)
	{
		this.Direction = direction;
		this.UpdateAnimationState();
	}

	/// <summary>
	/// Called when the <see cref="Avatar"/> becomes player's focus
	/// </summary>
	void IAvatar.HandlePlayerFocus()
	{
	}

	/// <summary>
	/// Called when the <see cref="Avatar"/> is no longer the player's focus.
	/// </summary>
	void IAvatar.HandlePlayerUnfocus()
	{
	}

	#endregion

	#region Callbacks

	/// <summary>
	/// Called on Awake
	/// </summary>
	protected virtual void OnAwake()
	{
	}

	/// <summary>
	/// Called on Update
	/// </summary>
	protected virtual void OnUpdate()
	{
	}

	/// <summary>
	/// Called when spawned
	/// </summary>
	/// <param name="position"></param>
	protected virtual void OnAvatarSpawn(Vector3 position)
	{
	}

	/// <summary>
	/// Called when destroyed
	/// </summary>
	protected virtual void OnAvatarDestroy()
	{
		this.DestroyGameObject();
	}

	/// <summary>
	/// Called after setting the owner
	/// </summary>
	/// <param name="newOwner"></param>
	protected virtual void OnSetOwner(MmoObject newOwner)
	{
	}

	/// <summary>
	/// Called when the player focuses on this <see cref="Avatar"/>.
	/// </summary>
	protected virtual void OnPlayerFocus()
	{
	}

	/// <summary>
	/// Called when the player loses focuses from this <see cref="Avatar"/>.
	/// </summary>
	protected virtual void OnPlayerUnfocus()
	{
	}

	/// <summary>
	/// Called on mouse over
	/// </summary>
	protected virtual void OnBeginHighlight()
	{
	}

	/// <summary>
	/// Called on mouse leave
	/// </summary>
	protected virtual void OnEndHighlight()
	{
	}

	#endregion

	#region Local Methods

	/// <summary>
	/// Called automatically when Primary Action is triggered
	/// </summary>
	void OnInteract()
	{
		if (allowInteraction)
			owner.World.Player.TryInteract(owner);
	}
	
	/// <summary>
	/// Called automatically when Secondary Action is triggered
	/// </summary>
	void OnTarget()
	{
		if (allowInteraction)
			owner.World.Player.TryTarget(owner);
	}
	
	/// <summary>
	/// Destroys all created <see cref="GameObject"/>.
	/// </summary>
	protected virtual void DestroyGameObject()
	{
		if(gameObject != null)
			Destroy(gameObject);
	}

	/// <summary>
	/// Called when the state or direction is changed
	/// </summary>
	protected virtual void UpdateAnimationState()
	{
	}

	#endregion

	#region MonoBehaviour Methods

	void Awake()
	{
		this.ObjectRenderer = this.GetComponentsInChildren<Renderer>().FirstOrDefault(r => r.tag == "mainRenderer");
		if (ObjectRenderer == null)
			this.ObjectRenderer = this.GetComponentInChildren<Renderer>();
		this.controller = this.GetComponentInChildren<CharacterController>();

		this.OnAwake();
	}

	void Update()
	{
		//if (InView)
		if(poiIconType != OverheadIconType.None)
			this.poiTransform.LookAt(Camera.main.transform);

		this.OnUpdate();
	}

	void OnMouseOver()
	{
		if (allowInteraction)
		{
			this.OnBeginHighlight();
			this.hover = true;
		}
	}

	void OnMouseExit()
	{
		if (allowInteraction)
		{
			this.OnEndHighlight();
			this.hover = true;
		}
	}

	void OnBecameVisible()
	{
		this.owner.OnBecameVisible();
		this.InView = true;
	}

	void OnBecameInvisible()
	{
		this.owner.OnBecameInvisible();
		this.InView = false;
	}

	#endregion
}
