﻿using System;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game
{
	public class WorldObjectCache
	{
		#region Constants and Fields

		/// <summary>
		/// caches
		/// </summary>
		private readonly StorageMap<byte, int, MmoObject> objects;

		#endregion

		#region Construtors and Destructors

		/// <summary>
		/// Creates a new instance of the <see cref="WorldObjectCache"/> class.
		/// </summary>
		public WorldObjectCache()
		{
			this.objects = new StorageMap<byte, int, MmoObject>();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Adds an item to the cache
		/// </summary>
		public bool AddItem(MmoObject mmoObject)
		{
			var guid = mmoObject.Guid;
			return objects.Add(guid.Type, guid.Id, mmoObject);
		}

		/// <summary>
		/// Removes an item from the cache
		/// </summary>
		public bool RemoveItem(MmoGuid guid)
		{
			return objects.Remove(guid.Type, guid.Id);
		}

		/// <summary>
		/// Tries to retrieve an item
		/// </summary>
		public bool TryGetItem(MmoGuid guid, out MmoObject mmoObject)
		{
			return objects.TryGetValue(guid.Type, guid.Id, out mmoObject);
		}

		/// <summary>
		/// Iterates through the collection
		/// </summary>
		/// <param name="method"></param>
		public void ForEach(Action<MmoObject> method)
		{
			this.objects.ForEach(method);
		}
		
		/// <summary>
		/// Destroys all MmoObjects
		/// </summary>
		public void Clear()
		{
			this.objects.ForEach(o => o.Destroy());
			this.objects.Clear();
		}

		#endregion
	}
}
