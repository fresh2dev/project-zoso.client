﻿namespace Karen90MmoFramework.Client.Game
{
	public enum InteractionWindowType : byte
	{
		WINDOW_NONE				= 0,
		WINDOW_INTERACTION_MENU	= 1,
		WINDOW_CONVERSATION		= 1,
		WINDOW_MERCHANT			= 2,
		WINDOW_QUEST_INTRO		= 3,
		WINDOW_QUEST_PROGRESS	= 4,
		WINDOW_QUEST_COMPLETE	= 5,
		WINDOW_LOOT				= 6,
	};
}