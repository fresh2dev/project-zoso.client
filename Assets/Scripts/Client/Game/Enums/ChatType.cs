﻿namespace Karen90MmoFramework.Client.Game
{
	public enum ChatType : byte
	{
		Normal = 0,
		Tell = 1,
		GmCommand = 2,
		PartyInvite = 3,
	}
}
