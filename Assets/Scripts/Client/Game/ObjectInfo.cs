﻿namespace Karen90MmoFramework.Client.Game
{
	public class ObjectInfo
	{
		/// <summary>
		/// Gets the extended name
		/// </summary>
		public string ExtendedName { get; set; }

		/// <summary>
		/// Gets or Sets the health info
		/// </summary>
		public string HealthInfo { get; set; }

		/// <summary>
		/// Gets or Sets the power info
		/// </summary>
		public string PowerInfo { get; set; }

		/// <summary>
		/// Gets or Sets the xp info
		/// </summary>
		public string XpInfo { get; set; }
	}
}
