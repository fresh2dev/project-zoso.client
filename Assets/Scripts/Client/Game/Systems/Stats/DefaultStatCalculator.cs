﻿using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Objects;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class DefaultStatCalculator : CharacterStatCalculator
	{
		#region Overrides of CharacterStatCalculator

		/// <summary>
		/// Calculates the resultant value of a <see cref="Stats"/>.
		/// </summary>
		public override short CalculateValue(Player player, Stats stat)
		{
			return 0;
		}

		#endregion
	}
}
