﻿using System.Collections.Generic;

namespace Karen90MmoFramework.Client.Game.Systems
{
	public class Group
	{
		#region Constants and Fields

		private readonly MmoGuid guid;
		private MmoGuid leaderGuid;

		private readonly List<GroupMember> members;

		#endregion

		#region Properties

		/// <summary>
		/// Gets the group guid
		/// </summary>
		public MmoGuid Guid
		{
			get
			{
				return guid;
			}
		}

		/// <summary>
		/// Gets the leader's guid
		/// </summary>
		public MmoGuid LeaderGuid
		{
			get
			{
				return this.leaderGuid;
			}
		}

		/// <summary>
		/// Gets the members' count
		/// </summary>
		public int Count
		{
			get { return this.members.Count; }
		}

		#endregion

		#region Constructors and Destructors

		public Group(MmoGuid guid, MmoGuid leaderGuid)
		{
			this.guid = guid;
			this.leaderGuid = leaderGuid;

			this.members = new List<GroupMember>();
		}

		#endregion

		#region Public Methods

		/// <summary>
		/// Gets all the members
		/// </summary>
		public IEnumerable<GroupMember> GetMembers()
		{
			return this.members;
		}

		/// <summary>
		/// Adds a member
		/// </summary>
		/// <param name="member"></param>
		public void AddMember(GroupMember member)
		{
			this.members.Add(member);
		}

		/// <summary>
		/// Removes a member
		/// </summary>
		/// <param name="guid"></param>
		public void RemoveMember(MmoGuid guid)
		{
			this.members.RemoveAll(m => m.Guid == guid);
		}

		/// <summary>
		/// Gets a member
		/// </summary>
		public GroupMember GetMember(MmoGuid guid)
		{
			return members.Find(m => m.Guid == guid);
		}

		#endregion
	}
}
