﻿using ExitGames.Client.Photon;

namespace Karen90MmoFramework.Client
{
	public interface IConnection
	{
		/// <summary>
		/// Gets the connection peer state
		/// </summary>
		PeerStateValue PeerState { get; }

		/// <summary>
		/// Connects to the server
		/// </summary>
		/// <param name="connectResponseHandler"></param>
		void Connect(IConnectResponseHandler connectResponseHandler);

		/// <summary>
		/// Updates the connection
		/// </summary>
		void Update();

		/// <summary>
		/// Disconnects the connection
		/// </summary>
		void Disconnect();
	}
}
