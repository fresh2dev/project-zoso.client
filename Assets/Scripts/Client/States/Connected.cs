﻿using System.Collections.Generic;

using ExitGames.Client.Photon;
using Karen90MmoFramework.Game;
using Karen90MmoFramework.Rpc;
using Karen90MmoFramework.Hud;

namespace Karen90MmoFramework.Client
{
	public class Connected : IConnectionState
	{
		#region Constants and Fields

		/// <summary>
		/// Gets the singleton instance
		/// </summary>
		public static readonly Connected Instance = new Connected();

		/// <summary>
		/// Gets the state
		/// </summary>
		public IConnectionState State
		{
			get
			{
				return this;
			}
		}

		#endregion

		#region IConnectionState Implementation

		/// <summary>
		/// Gets the connection state
		/// </summary>
		ConnectionStatus IConnectionState.Status
		{
			get
			{
				return ConnectionStatus.Connected;
			}
		}

		/// <summary>
		/// Handles a(n) <see cref="OperationResponse"/>.
		/// </summary>
		bool IConnectionState.HandleOperationResponse(OperationResponse operationResponse)
		{
			return false;
		}

		/// <summary>
		/// Handles a(n) <see cref="EventData"/>.
		/// </summary>
		bool IConnectionState.HandleEvent(EventData eventData)
		{
			switch ((ClientEventCode)eventData.Code)
			{
				case ClientEventCode.ChatChannelJoined:
					HandleEventChatChannelJoined(eventData);
					return true;

				case ClientEventCode.ChatChannelLeft:
					HandleEventChatChannelLeft(eventData);
					return true;

				case ClientEventCode.ChatMessageReceived:
					HandleEventChatMessageReceived(eventData);
					return true;

				case ClientEventCode.ObjectDestroyed:
				case ClientEventCode.ObjectSubscribed:
				case ClientEventCode.ObjectUnsubscribed:
				case ClientEventCode.ObjectMovement:
				case ClientEventCode.ObjectTransform:
				case ClientEventCode.ObjectProperty:
				case ClientEventCode.ObjectPropertyMultiple:
				case ClientEventCode.ObjectFlagsSet:
				case ClientEventCode.ObjectFlagsUnset:
				case ClientEventCode.ObjectLevelUp:
				case ClientEventCode.YouLevelUp:
				case ClientEventCode.InteractionShop:
				case ClientEventCode.InteractionShopList:
				case ClientEventCode.InteractionLoot:
				case ClientEventCode.InteractionLootList:
				case ClientEventCode.InteractionDialogue:
				case ClientEventCode.InteractionDialogueList:
				case ClientEventCode.InventoryInit:
				case ClientEventCode.InventoryItemAdded:
				case ClientEventCode.InventoryItemAddedMultiple:
				case ClientEventCode.InventoryItemRemoved:
				case ClientEventCode.InventoryItemMoved:
				case ClientEventCode.ActionbarInit:
				case ClientEventCode.SpellManagerInit:
				case ClientEventCode.SpellAdded:
				case ClientEventCode.SpellRemoved:
				case ClientEventCode.SpellCastBegin:
				case ClientEventCode.SpellCastEnd:
				case ClientEventCode.SpellCooldownBegin:
				case ClientEventCode.SpellCooldownEnd:
				case ClientEventCode.LootInit:
				case ClientEventCode.LootClear:
				case ClientEventCode.LootItemRemoved:
				case ClientEventCode.LootGoldRemoved:
				case ClientEventCode.QuestManagerInit:
				case ClientEventCode.QuestGiverStatus:
				case ClientEventCode.QuestStarted:
				case ClientEventCode.QuestFinished:
				case ClientEventCode.QuestProgress:
				case ClientEventCode.QuestRegress:
				case ClientEventCode.QuestAdded:
				case ClientEventCode.SocialFriendAddedName:
				case ClientEventCode.SocialFriendAddedNameMultiple:
				case ClientEventCode.SocialFriendAddedData:
				case ClientEventCode.SocialFriendAddedDataMultiple:
				case ClientEventCode.SocialIgnoreAddedName:
				case ClientEventCode.SocialIgnoreAddedNameMultiple:
				case ClientEventCode.SocialProfileUpdate:
				case ClientEventCode.SocialProfileUpdateMultiple:
				case ClientEventCode.SocialProfileRemoved:
				case ClientEventCode.SocialProfileStatus:
				case ClientEventCode.SocialFriendRequestReceived:
				case ClientEventCode.SocialFriendRequestCancelled:
				case ClientEventCode.GroupInit:
				case ClientEventCode.GroupMemberAdded:
				case ClientEventCode.GroupMemberAddedGuid:
				case ClientEventCode.GroupMemberAddedProperties:
				case ClientEventCode.GroupMemberAddedInactive:
				case ClientEventCode.GroupMemberRemoved:
				case ClientEventCode.GroupMemberUpdate:
				case ClientEventCode.GroupMemberDisconnected:
				case ClientEventCode.GroupUninvited:
				case ClientEventCode.GroupDisbanded:
				case ClientEventCode.GroupInviteReceived:
				case ClientEventCode.GroupInviteCancelled:
				case ClientEventCode.GroupInviteDeclined:
					NetworkEngine.Instance.EnqueueMessageHandling(eventData, ConnectionStatus.WorldEntered);
					return true;

				default:
					return false;
			}
		}

		/// <summary>
		/// Sends an operation
		/// </summary>
		void IConnectionState.SendOperation(PhotonPeer peer, ClientOperationCode operationCode, Dictionary<byte, object> parameters, bool encrypt)
		{
			peer.OpCustom((byte)operationCode, parameters, true, 0, encrypt);
		}

		#endregion

		#region Event Handlers

		private static void HandleEventChatChannelJoined(EventData eventData)
		{
			var channelType = (ChannelType) eventData[(byte) ParameterCode.ChannelType];
			var channelName = string.Empty;

			object param;
			if (eventData.Parameters.TryGetValue((byte) ParameterCode.ChannelName, out param))
				channelName = (string) param;

			ChatManager.Instance.OnJoinedChannel(channelType, channelName);
		}

		private static void HandleEventChatChannelLeft(EventData eventData)
		{
			var channelType = (ChannelType) eventData[(byte) ParameterCode.ChannelType];
			var channelName = string.Empty;

			object param;
			if (eventData.Parameters.TryGetValue((byte) ParameterCode.ChannelName, out param))
				channelName = (string) param;

			ChatManager.Instance.OnLeftChannel(channelType, channelName);
		}

		private static void HandleEventChatMessageReceived(EventData eventData)
		{
			var message = (string)eventData[(byte)ParameterCode.Message];
			var messageType = (MessageType)eventData[(byte)ParameterCode.MessageType];

			object param;
			if (eventData.Parameters.TryGetValue((byte)ParameterCode.Sender, out param))
			{
				var sender = (string)param;
				if (eventData.Parameters.TryGetValue((byte)ParameterCode.Receiver, out param))
				{
					var receiver = (string)param;
					ChatManager.Instance.PostMessage(messageType, message, sender, receiver);
				}
				else
				{
					ChatManager.Instance.PostMessage(messageType, message, sender);
				}
			}
			else
			{
				ChatManager.Instance.PostMessage(messageType, message);
			}
		}

		#endregion
	}
}