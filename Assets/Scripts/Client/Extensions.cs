﻿using Karen90MmoFramework.Rpc;

namespace Karen90MmoFramework.Client
{
	public static class Extensions
	{
		/// <summary>
		/// Gets the error message related to the <see cref="ResultCode"/>.
		/// </summary>
		/// <param name="returnCode"></param>
		/// <returns></returns>
		public static string GetErrorMessage(this ResultCode returnCode)
		{
			switch (returnCode)
			{
				case ResultCode.Ok:
					return "Ok";
				case ResultCode.Fail:
					return "Fail";
				case ResultCode.IncorrectUsernameOrPassword:
					return "Incorrect username or password";
				case ResultCode.CharacterNameAlreadyExists:
					return "Character name already exists";
				case ResultCode.UsernameAlreadyExists:
					return "Username already exists";
				case ResultCode.InsufficentFund:
					return "Insufficient fund";
				case ResultCode.InventoryFull:
					return "Inventory Full";
				case ResultCode.ObjectNotFound:
					return "Object not found";
				case ResultCode.ObjectIsOutOfRange:
					return "Object out of range";
				case ResultCode.ItemNotFound:
					return "Item not found";
				case ResultCode.PlayerNotFound:
					return "Player not found";
				case ResultCode.PlayerIsOffline:
					return "Player is offline";
				case ResultCode.PlayerIsBusy:
					return "Player is busy";
				case ResultCode.NoLootAvailable:
					return "No loot available";
				case ResultCode.InteractionNotAvailable:
					return "Cannot interact with this target";
				case ResultCode.CannotInviteSelf:
					return "Cannot invite self";
				case ResultCode.YourAlreadyInAGroup:
					return "You are already in a group";
				case ResultCode.PlayerAlreadyInAGroup:
					return "Player already in a group";
				case ResultCode.NoInvitationFound:
					return "No invitation found";
				case ResultCode.GroupIsFull:
					return "Group is full";
				case ResultCode.YouAreNotInAGroup:
					return "You are not in a group";
				case ResultCode.YouAreNotTheLeader:
					return "You are not the leader";
				case ResultCode.ItemUseFailed:
					return "Item cannot be used";
				case ResultCode.ObjectIsTooClose:
					return "Target is too close";
				case ResultCode.NotEnoughPower:
					return "Not enough power";
				case ResultCode.TargetIsDead:
					return "Target is dead";
				case ResultCode.CasterIsDead:
					return "You are dead";
				case ResultCode.TargetRequired:
					return "Target is required";
				case ResultCode.InvalidTarget:
					return "Invalid target";
				case ResultCode.SpellNotReady:
					return "Spell is not ready";
				case ResultCode.SpellNotFound:
					return "Spell not found";
				case ResultCode.SpellInCooldown:
					return "Spell is on cooldown";
				case ResultCode.AlreadyCasting:
					return "Cannot cast any spell now";
				default:
					return returnCode.ToString();
			}
		}
	}
}
