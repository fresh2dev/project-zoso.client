﻿using Karen90MmoFramework.Game;

namespace Karen90MmoFramework.Client
{
	public interface IChatHandler
	{
		/// <summary>
		/// Call this when the player joins a chat channel
		/// </summary>
		void OnJoinedChannel(ChannelType channelType, string channelName);

		/// <summary>
		/// Call this when the player leaves a chat channel
		/// </summary>
		void OnLeftChannel(ChannelType channelType, string channelName);

		/// <summary>
		/// Posts a chat message
		/// </summary>
		void PostMessage(MessageType messageType, string message);
		
		/// <summary>
		/// Posts a chat message from a user
		/// </summary>
		void PostMessage(MessageType messageType, string message, string sender);
	}
}
