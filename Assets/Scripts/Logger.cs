﻿/* Static logger can be used by any class without having to derive from monobehaviour
 * Have some debugging functionality
*/

using System.Diagnostics;
using System.IO;
using System;

public sealed class Logger
{
	private static readonly FileInfo _file = new FileInfo("log.log");
	private static Stopwatch _stopWatch;

	/// <summary>
	/// Logs
	/// </summary>
	public static void Debug(object content)
	{
		UnityEngine.Debug.Log(content);
	}

	/// <summary>
	/// Logs
	/// </summary>
	public static void DebugFormat(string content, params object[] args)
	{
		UnityEngine.Debug.Log(string.Format(content, args));
	}

	/// <summary>
	/// Logs a warning
	/// </summary>
	public static void Warning(object content)
	{
		UnityEngine.Debug.LogWarning(content);
	}

	/// <summary>
	/// Logs a swarning
	/// </summary>
	public static void WarningFormat(string content, params object[] args)
	{
		UnityEngine.Debug.LogWarning(string.Format(content, args));
	}

	/// <summary>
	/// Logs an error
	/// </summary>
	/// <param name="content"></param>
	public static void Error(object content)
	{
		UnityEngine.Debug.LogError(content);
	}

	/// <summary>
	/// Logs an error
	/// </summary>
	public static void ErrorFormat(string content, params object[] args)
	{
		UnityEngine.Debug.LogError(string.Format(content, args));
	}

	/// <summary>
	/// Logs the content to a log file in the path. Dont have to provide the name of the filename
	/// </summary>
	/// <param name="content"></param>
	/// <param name="overwrite"> </param>
	public static void LogToFile(string content, bool overwrite = false)
	{
		try
		{
			if (!_file.Exists || overwrite)
			{
				using (StreamWriter w = _file.CreateText())
				{
					w.WriteLine(getWriteContent(content));
					w.Close();
				}
			}
			else
			{
				using (StreamWriter w = _file.AppendText())
				{
					w.WriteLine(getWriteContent(content));
					w.Close();
				}
			}
		}
		catch (Exception e)
		{
			UnityEngine.Debug.LogError(e.Message);
		}
	}
	
	/// <summary>
	/// Starts a new timer and runs it
	/// </summary>
	public static void StartTimer()
	{
		try
		{
			_stopWatch = Stopwatch.StartNew();
			_stopWatch.Start();
		}
		catch (Exception e)
		{
			UnityEngine.Debug.Log(e.ToString());
		}
	}

	/// <summary>
	/// Stops the timer and returns the elapsed milliseconds
	/// </summary>
	/// <returns></returns>
	public static float StopTimer()
	{
		try
		{
			_stopWatch.Stop();
			return _stopWatch.ElapsedMilliseconds;
		}
		catch (Exception e)
		{
			UnityEngine.Debug.Log(e.ToString());
			return 0;
		}
	}

	private static string getWriteContent(string content)
	{
		return "\n<> Log - " + DateTime.Now.ToShortDateString() + "(" + DateTime.Now.ToShortTimeString() + ") : " + content + "\n";
	}
}
