﻿using Karen90MmoFramework.Client.Data;
using UnityEngine;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client;
using Karen90MmoFramework.Client.Game;
using Karen90MmoFramework.Client.Game.Items;
using Karen90MmoFramework.Client.Game.Systems;

namespace Karen90MmoFramework.Hud
{
	public partial class HUD
	{
		#region Constants and Fields

		struct DraggableMmoItem : IDraggable
		{
			#region Implementation of IDraggable

			/// <summary>
			/// Gets the item id
			/// </summary>
			public short ItemId { get; set; }

			/// <summary>
			/// Gets the drag texture
			/// </summary>
			public Texture2D Icon { get; set; }

			/// <summary>
			/// Gets the item type
			/// </summary>
			public DragItemType DragItemType
			{
				get
				{
					return DragItemType.MmoItem;
				}
			}

			#endregion
		}

		private Inventory inventory;

		private bool showInventory;
		private InventoryWindowType currentInventoryWindowType;

		private Texture2D textureInventoryGeneral;
		private Texture2D textureInventoryMisc;

		//private Texture2D textureCoin;
		private Texture2D textureItemHighlight;

		private Texture2D textureUselessGlow;
		private Texture2D textureUncommonGlow;
		private Texture2D textureRareGlow;
		private Texture2D textureSuperiorGlow;
		private Texture2D textureLegendaryGlow;
		private Texture2D textureAncestralGlow;

		private Rect rectInventoryWindow;

		private Rect rectInventoryGeneralTab;
		private Rect rectInventoryMiscTab;

		private Rect rectInventoryItem;
		private Rect rectInventorySpace;
		private Rect rectInventoryMoney;

		private int inventoryItemTopOffset;
		private int inventoryItemLeftOffset;
		private int inventoryItemSpacing;
		private int inventoryItemsPerColumn;

		private int? inventoryDragIndex;

		private string inventorySizeString;

		#endregion

		#region Public Methods

		/// <summary>
		/// Toggles inventory
		/// </summary>
		public void ToggleInventory()
		{
			if (showInventory)
				this.CloseInventory();
			else
				this.OpenInventory();
		}

		/// <summary>
		/// Opens the inventory
		/// </summary>
		public void OpenInventory()
		{
			if (!showInventory)
			{
				ShowWindow(this.handleInventoryWindow);

				this.OnButtonClick += this.HUD_OnInventoryButtonClick;
				this.OnDragDrop += this.HUD_OnInventoryDragDropped;

				this.showInventory = true;

				this.recalculateInventoryVals();
			}
		}
		
		/// <summary>
		/// Closes the inventory
		/// </summary>
		public void CloseInventory()
		{
			if (showInventory)
			{
				CloseWindow(this.handleInventoryWindow);
				this.OnButtonClick -= this.HUD_OnInventoryButtonClick;
				this.OnDragDrop -= this.HUD_OnInventoryDragDropped;

				this.showInventory = false;
			}
		}

		#endregion

		#region Local Methods

		private void initializeInventoryMembers()
		{
			this.textureInventoryGeneral = (Texture2D) Resources.Load("gui/win_inventory_general_s" + SkidId);
			this.textureInventoryMisc = (Texture2D) Resources.Load("gui/win_inventory_misc_s" + SkidId);

			//this.textureCoin = (Texture2D) Resources.Load("Icons/Coin");
			this.textureItemHighlight = (Texture2D) Resources.Load("Misc/Highlight");

			this.textureUselessGlow = (Texture2D) Resources.Load("Misc/rarityGlow_Useless");
			this.textureUncommonGlow = (Texture2D) Resources.Load("Misc/rarityGlow_Uncommon");
			this.textureRareGlow = (Texture2D) Resources.Load("Misc/rarityGlow_Rare");
			this.textureSuperiorGlow = (Texture2D) Resources.Load("Misc/rarityGlow_Superior");
			this.textureLegendaryGlow = (Texture2D) Resources.Load("Misc/rarityGlow_Legendary");
			this.textureAncestralGlow = (Texture2D) Resources.Load("Misc/rarityGlow_Ancestral");

			this.rectInventoryWindow = new Rect
				{
					x = (Screen.width - textureInventoryGeneral.width) - 10,
					y = (Screen.height - textureInventoryGeneral.height) / 2f,
					width = textureInventoryGeneral.width,
					height = textureInventoryGeneral.height
				};

			this.rectInventorySpace = new Rect
				{
					x = rectInventoryWindow.x + 60,
					y = rectInventoryWindow.yMax - 75,
					width = 100,
					height = 15
				};

			this.rectInventoryMoney = new Rect
				{
					x = rectInventoryWindow.xMax - 148,
					y = rectInventorySpace.y,
					width = 100,
					height = 15
				};

			this.rectInventoryItem = new Rect
				{
					x = 0,
					y = 0,
					width = GameSettings.ITEM_ICON_SIZE,
					height = GameSettings.ITEM_ICON_SIZE
				};

			this.rectInventoryGeneralTab = new Rect(rectInventoryWindow.x + 11, rectInventoryWindow.y + 468, 115, 31);
			this.rectInventoryMiscTab = new Rect(rectInventoryGeneralTab.xMax + 2, rectInventoryGeneralTab.y, rectInventoryGeneralTab.width, rectInventoryGeneralTab.height);

			this.inventoryItemsPerColumn = 10;
			this.inventoryItemSpacing = 46;
			this.inventoryItemTopOffset = (int) this.rectInventoryWindow.y + 54;
			this.inventoryItemLeftOffset = (int) this.rectInventoryWindow.x + 23;

			this.currentInventoryWindowType = InventoryWindowType.General;
			this.inventoryDragIndex = null;
		}

		private void handleInventoryContainer()
		{
			var slot = 0;
			for (var y = 0; slot < this.inventory.Size; y++)
			{
				for (var x = 0; x < this.inventoryItemsPerColumn; x++, slot++)
				{
					var slotslot = inventory[slot];
					if (slotslot.IsEmpty || this.inventoryDragIndex == slot)
						continue;

					var slotItem = slotslot.Item;
					var itemData = slotItem.ItemData;

					this.rectInventoryItem.y = this.inventoryItemTopOffset + this.inventoryItemSpacing * y;
					this.rectInventoryItem.x = this.inventoryItemLeftOffset + this.inventoryItemSpacing * x;

					var currEvent = Event.current;
					if (currEvent.type == EventType.MouseDrag && currEvent.button == GameSettings.LEFT_MOUSE_BUTTON && !this.IsDragging)
					{
						if (winControl == (int)WinId.WIN_ID_INVENTORY_GENERAL)
						{
							if (rectInventoryItem.Contains(currEvent.mousePosition))
								if (BeginDrag(new DraggableMmoItem{ItemId = itemData.ItemId, Icon = itemData.Icon}, ContainerType.Inventory))
									this.inventoryDragIndex = slot;
						}
					}

					GUIX.TooltipLabel(this.rectInventoryItem, itemData.DrawContent, this.guiSettings.GUIStyleButtonPlain);
					if (slotItem.Count > 1)
						GUIX.NumLabel(this.rectInventoryItem, slotItem.Count, this.guiSettings.GUIStyleLabelStackCount);

					if (slotItem.Rareness != Rarity.Common)
					{
						var glowTexture = this.textureUselessGlow;
						switch (slotItem.Rareness)
						{
							case Rarity.Uncommon:
								glowTexture = this.textureUncommonGlow;
								break;

							case Rarity.Rare:
								glowTexture = this.textureRareGlow;
								break;

							case Rarity.Superior:
								glowTexture = this.textureSuperiorGlow;
								break;

							case Rarity.Legendary:
								glowTexture = this.textureLegendaryGlow;
								break;

							case Rarity.Ancestral:
								glowTexture = this.textureAncestralGlow;
								break;
						}

						GUI.DrawTexture(new Rect(this.rectInventoryItem.x - 2, this.rectInventoryItem.y - 1, glowTexture.width, glowTexture.height), glowTexture);
					}

					if (this.rectInventoryItem.Contains(Event.current.mousePosition) && !this.IsDragging)
						GUI.DrawTexture(this.rectInventoryItem, this.textureItemHighlight);
				}
			}

			GUI.Label(rectInventorySpace, this.inventorySizeString, this.guiSettings.GUIStyleLabelInventoryLeft);
			GUIX.NumLabel(rectInventoryMoney, this.player.Money, this.guiSettings.GUIStyleLabelInventoryRight);
		}

		private void handleInventoryMisc()
		{
		}

		private void handleInventoryWindow()
		{
			switch (currentInventoryWindowType)
			{
				case InventoryWindowType.General:
					GUIX.Window((int) WinId.WIN_ID_INVENTORY_GENERAL, this.rectInventoryWindow, this.textureInventoryGeneral, this.guiSettings.GUIStyleWindow);

					this.handleInventoryContainer();

					if (GUIX.Button(this.rectInventoryMiscTab, "Misc", guiSettings.GUIStyleButtonTabs))
						this.currentInventoryWindowType = InventoryWindowType.Misc;
					break;

				case InventoryWindowType.Misc:
					GUIX.Window((int) WinId.WIN_ID_INVENTORY_MISC, this.rectInventoryWindow, this.textureInventoryMisc, this.guiSettings.GUIStyleWindow);

					this.handleInventoryMisc();

					if (GUIX.Button(this.rectInventoryGeneralTab, "General", guiSettings.GUIStyleButtonTabs))
					{
						this.currentInventoryWindowType = InventoryWindowType.General;
						this.recalculateInventoryVals();
					}
					break;
			}

			if (GUIX.Button(new Rect(rectInventoryWindow.xMax - closeButtonSize - closeButtonOffsetXMax, rectInventoryWindow.y + closeButtonOffsetY, closeButtonSize, closeButtonSize), string.Empty, guiSettings.GUIStyleButtonClose))
			{
				this.CloseInventory();
			}
		}

		private void HUD_OnInventoryDragDropped(Vector2 coord, ContainerType? containerType)
		{
			if (containerType.HasValue && containerType.Value == ContainerType.Inventory && currentInventoryWindowType == InventoryWindowType.General)
			{
				var i = (int) (coord.x - this.inventoryItemLeftOffset) / this.inventoryItemSpacing;
				var j = (int) (coord.y - this.inventoryItemTopOffset) / this.inventoryItemSpacing;

				this.rectInventoryItem.x = this.inventoryItemLeftOffset + this.inventoryItemSpacing * i;
				this.rectInventoryItem.y = this.inventoryItemTopOffset + this.inventoryItemSpacing * j;

				if (rectInventoryItem.Contains(coord))
				{
					var index = j * this.inventoryItemsPerColumn + i;
					if (inventory.MoveItem(index, this.inventoryDragIndex.Value))
						Operations.MoveInventoryItem((byte) index, (byte) inventoryDragIndex.Value);
				}
			}

			this.inventoryDragIndex = null;
		}

		private void HUD_OnInventoryButtonClick(int button, Vector2 coord)
		{
			// current window is not inventory ? return
			if (winControl != (int) WinId.WIN_ID_INVENTORY_GENERAL)
				return;
			// is the button not right mouse button ? return
			if (button != GameSettings.RIGHT_MOUSE_BUTTON)
				return;
			// figure out the click coordinate relative to the inventory rect
			var i = (int) (coord.x - this.inventoryItemLeftOffset) / this.inventoryItemSpacing;
			var j = (int) (coord.y - this.inventoryItemTopOffset) / this.inventoryItemSpacing;
			// calculate the slot rect
			this.rectInventoryItem.x = this.inventoryItemLeftOffset + this.inventoryItemSpacing * i;
			this.rectInventoryItem.y = this.inventoryItemTopOffset + this.inventoryItemSpacing * j;
			// is the click not within the slot rect ? return
			if (!rectInventoryItem.Contains(coord))
				return;
			// figure out the inventory slot
			var slot = j * this.inventoryItemsPerColumn + i;
			// is the slot not valid ? return
			if (slot < 0 || slot >= this.inventory.Size)
				return;
			// is the slot empty ? return
			var slotslot = this.inventory[slot];
			if (slotslot.IsEmpty)
				return;
			// since the slot is not empty there should be an item
			var itemAtSlot = slotslot.Item;
			// are we interacting with anything ?
			var interactor = this.player.CurrentInteractor;
			if (interactor != null)
			{
				// if the interactor is a merchant sell the item
				if (interactor.IsMerchant())
				{
					// request server to sell the item
					Operations.SellItem((byte) slot, (byte) itemAtSlot.Count);
					return;
				}
			}
			// if the item is usable request server to use the item
			if (itemAtSlot.IsUsable)
				Operations.UseInventoryItem((byte) slot, interactor != null ? interactor.Guid : (MmoGuid?) null);
		}

		private void recalculateInventoryVals()
		{
			if (showInventory)
				if (currentInventoryWindowType == InventoryWindowType.General)
					this.inventorySizeString = string.Format("{0} / {1}", inventory.FilledSlots, inventory.Size);
		}

		private void Inventory_OnItemAdded(IMmoItem mmoItem, int count, int slot)
		{
			this.recalculateInventoryVals();
		}

		private void Inventory_OnItemRemoved(IMmoItem mmoItem, int count, int slot)
		{
			this.recalculateInventoryVals();
		}

		#endregion
	}
}
