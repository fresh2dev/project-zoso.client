﻿using UnityEngine;

using System.Collections.Generic;

using Karen90MmoFramework.Game;
using Karen90MmoFramework.Client.Game.Systems;
using Karen90MmoFramework.Client.Data;
using Karen90MmoFramework.Client.Game;

namespace Karen90MmoFramework.Hud
{
	public partial class HUD
	{
		#region Constants and Fields

		private QuestManager questManager;

		private List<TrackerQuest> trackingQuests;

		#endregion

		#region Public Methods

		/// <summary>
		/// Automatically tracks a quest if the tracker isn't full
		/// </summary>
		public void AutoTrackQuest(short questId)
		{
			if (trackingQuests.Count < 5)
			{
				var tQuest = new TrackerQuest() { QuestId = questId };
				this.trackingQuests.Add(tQuest);
				this.UpdateQuest(tQuest);
			}
		}

		/// <summary>
		/// Tracks a specific quest and automatically untracks the first quest added
		/// </summary>
		public void TrackQuest(short questId)
		{
			if (trackingQuests.Count >= 5)
			{
				trackingQuests.RemoveAt(0);
			}

			var tQuest = new TrackerQuest() { QuestId = questId };
			this.trackingQuests.Add(tQuest);
			this.UpdateQuest(tQuest);
		}

		/// <summary>
		/// Untracks a specific quest
		/// </summary>
		public void UntrackQuest(short questId)
		{
			trackingQuests.RemoveAll(q => q.QuestId == questId);
		}

		/// <summary>
		/// Updates a certain quest for progress changes
		/// </summary>
		public void UpdateQuest(short questId)
		{
			var quest = this.trackingQuests.Find(q => q.QuestId == questId);
			if (quest.QuestId == questId)
			{
				this.UpdateQuest(quest);
			}
		}

		#endregion

		#region Local Methods

		void InitializeQuestTrackerMembers()
		{
			this.trackingQuests = new List<TrackerQuest>();
		}

		void HandleQuestTracker()
		{
			var height = rectMinimapContainer.yMax + 50;
			var x = Screen.width - 310;
			foreach (var quest in trackingQuests)
			{
				GUIX.LabelOutline(new Rect(x, height, 300, 18), quest.Name, guiSettings.GUIStyleLabelTrackerQuestName);
				height += 18;

				foreach (var objective in quest.Objectives)
				{
					GUIX.LabelOutline(new Rect(x, height, 300, 16), objective, guiSettings.GUIStyleLabelTrackerObjective);
					height += 16;
				}

				height += 10;
			}
		}

		void UpdateQuest(TrackerQuest tQuest)
		{
			var questId = tQuest.QuestId;

			QuestData questData;
			if (MmoItemDataStorage.Instance.TryGetQuest(questId, out questData))
			{
				var activeQuest = questManager.GetQuest(questId);
				if (tQuest.Objectives == null)
				{
					tQuest.Name = questData.Name + " [" + questData.Level + "]";

					var qdObj = questData.Objectives;
					var objs = new GUIContent[qdObj.Length];
					for (int i = 0; i < qdObj.Length; i++)
					{
						string objective = string.Empty;
						if (activeQuest.Status == QuestStatus.TurnIn)
						{
							objs = new[] { new GUIContent(questData.FinalObjective) };
							break;
						}
						else
						{
							var reqCount = questData.ObjectiveCounts[i];
							var actCount = activeQuest.Counts[i];

							if (reqCount == actCount)
								objective = "(C) ";

							if (reqCount > 1)
							{
								objective += qdObj[i] + ": " + actCount + " / " + reqCount;
							}
							else
							{
								objective += qdObj[i];
							}
						}
						
						objs[i] = new GUIContent(objective);
					}

					tQuest.Objectives = objs;
				}
				else
				{
					var qdObj = questData.Objectives;
					var objs = new GUIContent[qdObj.Length];
					for (int i = 0; i < qdObj.Length; i++)
					{
						string objective = string.Empty;
						if (activeQuest.Status == QuestStatus.TurnIn)
						{
							objs = new[] { new GUIContent(questData.FinalObjective) };
							break;
						}
						else
						{
							var reqCount = questData.ObjectiveCounts[i];
							var actCount = activeQuest.Counts[i];

							if (reqCount == actCount)
								objective = "(C) ";

							if (reqCount > 1)
							{
								objective += qdObj[i] + ": " + actCount + " / " + reqCount;
							}
							else
							{
								objective += qdObj[i];
							}
						}

						objs[i] = new GUIContent(objective);
					}

					tQuest.Objectives = objs;
				}
			}
		}

		#endregion

		class TrackerQuest
		{
			public short QuestId { get; set; }
			public string Name { get; set; }
			public GUIContent[] Objectives { get; set; }
		}
	}
}
