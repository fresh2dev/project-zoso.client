﻿using UnityEngine;

using System.Text;
using System;

public static class Extensions
{
	private static readonly char[] _splitChars = new[] {' '};

	/// <summary>
	/// Caps the first letter of every word in a given text
	/// </summary>
	public static string CapFirstLetter(this string text)
	{
		if (string.IsNullOrEmpty(text))
			return string.Empty;

		var returnString = new StringBuilder();
		var splits = text.Split(_splitChars, StringSplitOptions.RemoveEmptyEntries);

		if (splits.Length > 1)
		{
			foreach (var word in splits)
			{
				returnString.Append(char.ToUpper(word[0])).Append(word.Substring(1).ToLower()).Append(' ');
			}

			// removing the trailing space
			returnString = returnString.Remove(returnString.Length - 1, 1);
		}
		else
		{
			returnString = returnString.Append(char.ToUpper(text[0])).Append(text.Substring(1).ToLower());
		}

		return returnString.ToString();
	}

	/// <summary>
	/// Gets the distance squared. Much faster than taking a square root. Use it whenever the distance isnt necessary.
	/// </summary>
	public static float GetDistanceSquared(this Vector3 p1, Vector3 p2)
	{
		var dX = (p2.x - p1.x);
		var dY = (p2.y - p1.y);
		var dZ = (p2.z - p1.z);

		return dX*dX + dY*dY + dZ*dZ;
	}

	/// <summary>
	/// Gets the distance between two vectors
	/// </summary>
	public static float GetDistance(this Vector3 p1, Vector3 p2)
	{
		var dX = (p2.x - p1.x);
		var dY = (p2.y - p1.y);
		var dZ = (p2.z - p1.z);

		return Mathf.Sqrt(dX*dX + dY*dY + dZ*dZ);
	}

	/// <summary>
	/// Converts the Enum to String.
	/// </summary>
	/// <remarks> Faster than invoking ToString </remarks>
	public static string ToEnumString(this Enum enumValue)
	{
		return Enum.GetName(enumValue.GetType(), enumValue);
	}

	/// <summary>
	/// Converts a vector value to float array
	/// </summary>
	public static float[] ToFloatArray(this Vector3 vector, byte dimensions)
	{
		switch (dimensions)
		{
			case 0:
				return new float[0];

			case 1:
				return new[] {Convert.ToSingle(vector.x)};

			case 2:
				return new[] {Convert.ToSingle(vector.x), Convert.ToSingle(vector.z)};

			case 3:
				return new[] { Convert.ToSingle(vector.x), Convert.ToSingle(vector.y), Convert.ToSingle(vector.z) };

			default:
				throw new ArgumentOutOfRangeException("dimensions");
		}
	}

	/// <summary>
	/// Converts a float array to vector
	/// </summary>
	/// <param name="coordinate"></param>
	/// <returns></returns>
	public static Vector3 ToVector(this float[] coordinate)
	{
		switch (coordinate.Length)
		{
			case 0:
				return new Vector3();

			case 1:
				return new Vector3 { x = coordinate[0] };

			case 2:
				return new Vector3
					{
						x = coordinate[0],
						y = coordinate[1]
					};

			default:
				return new Vector3
					{
						x = coordinate[0],
						y = coordinate[1],
						z = coordinate[2],
					};
		}
	}

	/// <summary>
	/// Gets the distance using x and z coord
	/// </summary>
	public static float FlatDistance(this Vector3 from, Vector3 to)
	{
		var xDif = from.x - to.x;
		var zDif = from.z - to.z;

		return (float)Math.Sqrt(xDif * xDif + zDif * zDif);
	}
}
